---
title: Safety on Construction Site
description: Reduce injuries on the construction site by using computer vision alghoritms
author: Camilo, Mia, Sushmitha
keywords: marp,marp-cli,slide
url: Please replace this with the URL of the deployed presentation
marp: true
image: Please replace this with a URL of an avatar/icon of your presentation
---

# My Validated Concept
## It will crush the world

---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>


# The Problem 
 
What does it mean for someone to work in the construction industry?
- 75% chances of experience a disabling injury during the average carreer time (45 years)
- 71% greater chance of injury than in all the other industries as a whole
- 25% of fatal accidents at work in the EU took place in the construction industry

---
# The Problem

What does it mean for a company having someone injured in the Construction Site?
- The average cost non fatal injury is *1.100 USD*. *41.000 USD* if it involves medical care. *1.190.000* if it's a fatal accident.
- For every dollar spent in injuries in direct costs, the indirect costs is 2.12 USD
- 15% of all workers compensation is spent on workers who were injured at the construction site

---
# The Market

![](./assets/market_size.png)

---
# Our Solution

> Site Monitoring System for Predictive Risk Mitigation 

Using existing Software 

# Checklists

These are always helpful
- [x] _==&leftarrow; A highlighted, right-floating segment==_{.float-right}Done
- [x] _==&leftarrow; A highlighted, right-floating segment==_{.float-right}Done
- [ ] _==&leftarrow; A highlighted, right-floating segment==_{.float-right}Still to be done
- [ ] _==&leftarrow; A highlighted, right-floating segment==_{.float-right}Still to be done

---

# Regular slides are okay

> Quoted blocks are fancy, they add context

Paragraphs get to the point :) (check emoji textual representations [here](https://github.com/markdown-it/markdown-it-emoji/blob/master/lib/data/shortcuts.js))

---

# Mindmaps

But also, any **plantuml** diagram...

::: fence
@startuml

@startmindmap
* Big idea
** Smaller idea
*** Even smaller idea
*** ...
*** ...
** ...
** ...
@endmindmap

@enduml
:::
