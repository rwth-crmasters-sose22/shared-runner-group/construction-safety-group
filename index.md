---
title: Safety on Construction Site
description: Reduce injuries on the construction site by using computer vision alghoritms
author: Camilo, Mia, Sushmitha
keywords: marp,marp-cli,slide
url: Please replace this with the URL of the deployed presentation
marp: true
image: Please replace this with a URL of an avatar/icon of your presentation
---

# Monitoring on Construction Site

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

---

# The Stories
 
- It's very difficult to monitor the compliance of Construction Worker with PPE regulations, we have to go around the construction site and see everyone to verify compliance. - Ocupational Hazard Engineer 
- Human - Machine Accidents are one of the most dangerous in the Construction Site. If you are not dangerous you can kill someone. - Machine Operator
- There's a lot of noise in a construction site wich causes construction workes to not always be aware of their sorroundings and the hazards around them. - Site Engineer 
---


# Mind Map

::: fence 
@startuml 
@startmindmap
+[#orange] Computer Vision in Construction Site
++[#orange] Reduce Risk
+++[#orange] Humans-Machine Interaction in the Construction Site 
++++[#orange] Human Machine Distance Detection
++++ Object Camera Distance Detection
+++[#orange] Human by Itself Interaction 
++++[#orange] PPE Detection
+++++[#orange] Vest Detection
+++++[#orange] Helmet Detection
+++++[#orange] Goggles Detection
++++ Human Recognition
+++++ Hand Gesture Recognition
+++++ Construction Worker Recognition
+++++ Construction Worker Counting
+++ Other Hazards
++++ Fire Detection System
++ Optimize
+++ Vehicles
++++ Plate Recognition System
++++ Vehicle Counting
++++ Gesture Recognition
+++ Supply Chain Enhancement
++++ Material Counting 
++++ Material Tracking
@endmindmap

@enduml
:::

---

# Open Source Repositories

- [PPE Detection](https://github.com/mdfaiz99/Personal-Protective-Equipment-Detection-using-Computer-Vision) -- Detects that construction workers are wearing the adequate PPE. Inputs an image and returns a vector of bounding boxes and class predictions. Has Live Streaming Capabilities
- [Collision Safety System](https://github.com/bharath96sripathy/Collision-safety-system-using-computer-vision) -- Uses Face Detection to send a signal to a vehicle in case it's in collision course with a human. 

---

# Locked Concept

- Using exisiting site monitoring systems. We are going to implement computer vision algorithms to enhance safety in the construction site.
- We'll implent the PPE Recognition algorithm to send a signal over to the Ocupational Safety Responsible in case of Vialotions. And also store the data to generate reports around this infractions. 
- We'll implement the Collision Safety Algorithm to generate a signal once one vehicle is getting closer in collision course to a person. This could potentially lead to the machine to be stopped inmeaadiatly based on this signal. 


---


# Validation 

Opinions: 
- Location of Cameras is Key for this concept to work
- Increase in site safety spending due to work regulations is a huge opportunity
- Review what other algorithms could be implemented for future features


---
# The Market

![](./assets/market_size.png)

---
# Our Solution

> Site Monitoring System for Predictive Risk Mitigation 

Leveraging from existing site monitoring infrastructure (or provide one if needed) use computer vision alghoritms to detect and alert about safety compliance issues.

Examples: 
- People too close to machinery
- People not wearing the appropiate safety equipment
- People performing activities in an unsafely way

---

# How are we going to execute?
- Python
- Open CV
- Yolo
- Webcam
---
# Exisiting Repositories

- [Safety Equipment Check](https://github.com/mdfaiz99/Personal-Protective-Equipment-Detection-using-Computer-Vision)
- [Helmet Detection](https://github.com/wujixiu/helmet-detection)
- [Construction Safety Measure Detection](https://github.com/nirmalsenthilnathan/Construction-Safety-Measure-Detection-Model-with-Evaluation-metric)
- [Vehicle Distance Detection](https://github.com/Gaurav-1213/Vehicle_Object_Detection-using-Keras)


---
# Comments 

